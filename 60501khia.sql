-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 09, 2020 at 08:46 PM
-- Server version: 8.0.21-0ubuntu0.20.04.4
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `60501khia`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `id` int NOT NULL COMMENT 'ID матча',
  `id_team1` int NOT NULL COMMENT 'ID команды 1',
  `id_team2` int NOT NULL COMMENT 'ID команды 2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`id`, `id_team1`, `id_team2`) VALUES
(1, 8, 5),
(2, 12, 18),
(3, 14, 9),
(4, 1, 6),
(5, 16, 17),
(6, 15, 4),
(7, 19, 11),
(8, 10, 3),
(9, 2, 20),
(10, 7, 13),
(11, 4, 14),
(12, 9, 8),
(13, 5, 19),
(14, 3, 16),
(15, 20, 12),
(16, 17, 15),
(17, 13, 10),
(18, 6, 7),
(19, 18, 2),
(20, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `goal`
--

CREATE TABLE `goal` (
  `id` int NOT NULL COMMENT 'ID гола',
  `id_game` int NOT NULL COMMENT 'ID матча',
  `id_player` int NOT NULL COMMENT 'ID игрока',
  `goaltime` int NOT NULL COMMENT 'Время от начала матча'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goal`
--

INSERT INTO `goal` (`id`, `id_game`, `id_player`, `goaltime`) VALUES
(1, 1, 8, 33),
(2, 2, 18, 74),
(3, 3, 14, 15),
(4, 4, 1, 86),
(5, 5, 16, 21),
(6, 6, 4, 67),
(7, 7, 11, 43),
(8, 8, 10, 12),
(9, 9, 20, 78),
(10, 10, 7, 43),
(11, 11, 4, 24),
(12, 12, 9, 8),
(13, 13, 19, 56),
(14, 14, 16, 78),
(15, 15, 20, 16),
(16, 16, 17, 37),
(17, 17, 13, 81),
(18, 18, 7, 28),
(19, 19, 2, 11),
(20, 20, 11, 72);

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `id` int NOT NULL COMMENT 'ID игрока',
  `id_team` int NOT NULL COMMENT 'ID команды',
  `name` varchar(255) NOT NULL COMMENT 'ФИО игрока',
  `amplua` varchar(255) NOT NULL COMMENT 'Амплуа игрока'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`id`, `id_team`, `name`, `amplua`) VALUES
(1, 1, 'Александр Ляказет', 'НАП'),
(2, 2, 'Джек Грилиш', 'ЦАП'),
(3, 3, 'Крис Вуд', 'НАП'),
(4, 4, 'Нил Мопей', 'НАП'),
(5, 5, 'Матеус Перейра', 'НАП'),
(6, 6, 'Джаррод Боуэ', 'НАП'),
(7, 7, 'Адама Траоре', 'ПВ'),
(8, 8, 'Ришарлисон', 'НАП'),
(9, 9, 'Уилфрид Заха', 'ЛВ'),
(10, 10, 'Джейми Варди', 'НАП'),
(11, 11, 'Диогу Жота', 'ЛВ'),
(12, 12, 'Джек Харрисон', 'ПП'),
(13, 13, 'Рахим Стерлинг', 'ЛВ'),
(14, 14, 'Бруну Фернандеш', 'ЦАП'),
(15, 15, 'Аллен Сен-Максимен', 'ПВ'),
(16, 16, 'Денни Ингз', 'НАП'),
(17, 17, 'Сон Хын Мин', 'ЛВ'),
(18, 18, 'Александар Митрович', 'НАП'),
(19, 19, 'Тамми Абрахам', 'НАП'),
(20, 20, 'Дэвид Макголдрик', 'НАП');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int NOT NULL COMMENT 'ID команды',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Наименование команды'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`) VALUES
(1, 'Арсенал'),
(2, 'Астон Вилла'),
(3, 'Бернли'),
(4, 'Брайтон'),
(5, 'Вест Бромвич'),
(6, 'Вест Хэм'),
(7, 'Вулверхэмптон'),
(8, 'Эвертон'),
(9, 'Кристал Пэлас'),
(10, 'Лестер Сити'),
(11, 'Ливерпуль'),
(12, 'Лидс'),
(13, 'Манчестер Сити'),
(14, 'Манчестер Юнайтед'),
(15, 'Ньюкасл'),
(16, 'Саутгемптон'),
(17, 'Тоттенхэм'),
(18, 'Фулхэм'),
(19, 'Челси'),
(20, 'Шеффилд Юнайтед');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_ibfk_1` (`id_team1`),
  ADD KEY `game_ibfk_2` (`id_team2`);

--
-- Indexes for table `goal`
--
ALTER TABLE `goal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_game` (`id_game`),
  ADD KEY `id_player` (`id_player`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_team` (`id_team`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID матча', AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `goal`
--
ALTER TABLE `goal`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID гола', AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID игрока', AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID команды', AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_ibfk_1` FOREIGN KEY (`id_team1`) REFERENCES `team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `game_ibfk_2` FOREIGN KEY (`id_team2`) REFERENCES `team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `goal`
--
ALTER TABLE `goal`
  ADD CONSTRAINT `goal_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `game` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `goal_ibfk_2` FOREIGN KEY (`id_player`) REFERENCES `player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `player`
--
ALTER TABLE `player`
  ADD CONSTRAINT `player_ibfk_1` FOREIGN KEY (`id_team`) REFERENCES `team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
