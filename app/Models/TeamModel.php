<?php namespace App\Models;
use CodeIgniter\Model;
class TeamModel extends Model
{
    protected $table = 'team'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id', 'name', 'picture_team'];
    public function getTeam($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}
