<?php namespace App\Models;
use CodeIgniter\Model;
class GameModel extends Model
{
    protected $table = 'game'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id', 'id_team1', 'id_team2'];
    
    public function getGameEdit($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getGame($id = null)
    {

        $builder = $this->from([], true)
                        ->select('q,w,e,r,t,y,u')->from('games_score()')->where('q', $id);

        if (!is_null($id))
        {
            return $builder->where(['q' => $id])->first();
        }
        return $builder;
    }

    public function getGameWithTeam($id = null, $search = '')
    {
        /*$builder = $this->from([], true)
                        ->select('G.id AS Q, T1.name AS W, T1.picture_team AS E, T2.picture_team AS R, T2.name AS T')->from('game AS G')
                        ->join('team AS T1', 'T1.id = G.id_team1')
                        ->join('team AS T2', 'T2.id = G.id_team2')
                        ->like('T1.name', $search)->orLike('T2.name',$search);*/
        /*$builder = $this->from([], true)
                        ->select('G.id AS Q, G.id_team1, G.id_team2, Z.name AS R, X.name AS T, Z.picture_team AS Y, X.picture_team AS U,
(select count(*) AS W from goal, game, team, player
WHERE game.id=goal.id_game AND game.id_team1=team.id and goal.id_game = G.id and team.id = G.id_team1 and goal.id_player=player.id and player.id_team=team.id),
(select count(*) AS E from goal, game, team, player
WHERE game.id=goal.id_game AND game.id_team2=team.id and goal.id_game = G.id and team.id = G.id_team2 and goal.id_player=player.id and player.id_team=team.id)
from game as G, team as Z, team as X WHERE Z.id=G.id_team1 AND X.id=G.id_team2', false);*/

        $builder = $this->from([], true)
                        ->select('q,w,e,r,t,y,u')->from('games_score()')->like('r', $search,'both', null, true)->orLike('t',$search,'both',null,true);

        if (!is_null($id))
        {
            return $builder->where(['q' => $id])->first();
        }
        return $builder;
    }
}
