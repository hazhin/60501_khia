<?php namespace App\Models;
use CodeIgniter\Model;
class PlayerModel extends Model
{
    protected $table = 'player'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id', 'id_team', 'name', 'amplua', 'picture_url'];
    public function getPlayer($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
    public function getPlayerByTeamId($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id_team' => $id])->findAll();
    }

    public function getPlayerWithTeam($id = null, $search = '')
    {
        $builder = $this->from([], true)
                        ->select('p.id AS p1, p.picture_url AS p2, p.name AS p3, t.picture_team AS t2, t.name AS t3, p.amplua AS p4')->from('player AS p')
                        ->join('team AS t', 't.id = p.id_team')
                        ->like('p.name', $search,'both', null, true)->orLike('t.name', $search,'both', null, true);

        if (!is_null($id))
        {
            //return $results;//->where(['team.id' => $id])->first();
            return $builder->where(['p.id' => $id])->first();
        }
        //return $results;
        return $builder;
    }
}
