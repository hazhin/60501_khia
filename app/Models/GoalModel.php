<?php namespace App\Models;
use CodeIgniter\Model;
class GoalModel extends Model
{
    protected $table = 'goal'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id', 'id_game', 'id_player', 'goaltime'];
    
    public function getGoal($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getGoalByGameId($id = null)
    {

        $builder = $this->from([], true)
                        ->select('q,w,e,r,t,y')->from('goal_score()')->where('q', $id);

        return $builder->findAll();
    }

    public function goalEdit($id = null)
    {

        $builder = $this->from([], true)
                        ->select('q,w,e,r')->from('goal_edit()')->where('r', $id);

        return $builder->findAll();
    }

    public function getGoalView($id = null, $search = '')
    {

        $builder = $this->from([], true)
                        ->select('g.id_game as q, p.name as w, p.picture_url as e, p.amplua as r, g.goaltime as t, g.id as y') 
                        ->from('goal as g')
                        ->join('player as p', 'p.id=g.id_player')
                        ->join('team as t', 't.id=p.id_team')
                        ->orderBy('q', 'ASC')
                        ->like('p.name', $search,'both', null, true);

        if (!is_null($id))
        {
            return $builder->where(['g.id_game' => $id])->first();
        }
        return $builder;
    }

}
