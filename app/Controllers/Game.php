<?php namespace App\Controllers;

use App\Models\TeamModel;
use App\Models\GameModel;
use App\Models\GoalModel;
use CodeIgniter\Controller;

class Game extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GameModel();
        $data ['game'] = $model->getGame();
        echo view('game/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GameModel();
        $data ['game'] = $model->getGame($id);
        $model = new GoalModel();
        $data ['goal'] = $model->getGoalByGameId($id);
        echo view('game/view', $this->withIon($data));
    }

    public function viewAllWithTeam()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);

            $model = new GameModel();
            $data['game'] = $model->getGameWithTeam(null, $search)->paginate($per_page, 'group2');
            $data['pager'] = $model->pager;
            echo view('game/view_all_with_team', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Необходимы права администратора!'));
            return redirect()->to('/auth/login');
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new TeamModel();
        $data ['team'] = $model->getTeam();
        $data ['validation'] = \Config\Services::validation();
        echo view('game/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'id_team1'  => 'required',
                'id_team2'  => 'required',
            ]))
        {

            $model = new GameModel();
            //подготовка данных для модели
            $data = [
                'id_team1' => $this->request->getPost('id_team1'),
                'id_team2' => $this->request->getPost('id_team2'),
            ];

            $model->save($data);
            session()->setFlashdata('message', lang('Матч был успешно добавлен!'));
            return redirect()->to('/game/viewAllWithTeam');
        }
        else
        {
            return redirect()->to('/game/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GameModel();
        
        helper(['form']);
        $data ['game'] = $model->getGameEdit($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('game/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);
        echo '/game/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id_team1'  => 'required',
                'id_team2'  => 'required',
            ]))
        {

            $model = new GameModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'id_team1' => $this->request->getPost('id_team1'),
                'id_team2' => $this->request->getPost('id_team2'),
            ];

            $model->save($data);
            session()->setFlashdata('message', lang('Матч был успешно отредактирован!'));
            return redirect()->to('/game/view/'.$this->request->getPost('id'));
        }
        else
        {
            return redirect()->to('/game/edit/'.$this->request->getPost('id'))->withInput();
        }
    }
    
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GameModel();
        $model->delete($id);
        session()->setFlashdata('message', lang('Матч был успешно удален!'));
        return redirect()->to('/game/viewAllWithTeam');
    }
}
