<?php namespace App\Controllers;

use App\Models\GoalModel;
use CodeIgniter\Controller;

class Goal extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GoalModel();
        $data ['goal'] = $model->getGoal();
        echo view('goal/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GoalModel();
        $data ['goal'] = $model->getGoal($id);
        echo view('goal/view', $this->withIon($data));
    }

    public function viewAllWithGoal()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);

            $model = new GoalModel();
            $data['goal'] = $model->getGoalView(null, $search)->paginate($per_page, 'group3');
            $data['pager'] = $model->pager;
            echo view('goal/view_all_with_goal', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Необходимы права администратора!'));
            return redirect()->to('/auth/login');
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('goal/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'id_game'  => 'required',
                'id_player'  => 'required',
                'goaltime'  => 'required',
            ]))
        {

            $model = new GoalModel();
            //подготовка данных для модели
            $data = [
                'id_game' => $this->request->getPost('id_game'),
                'id_player' => $this->request->getPost('id_player'),
                'goaltime' => $this->request->getPost('goaltime'),
            ];

            $model->save($data);
            session()->setFlashdata('message', lang('Гол был успешно добавлен!'));
            return redirect()->to('/game/view/'.$this->request->getPost('id_game'));
        }
        else
        {
            return redirect()->to('/goal/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GoalModel();
        
        helper(['form']);
        $data ['goal'] = $model->goalEdit($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('goal/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);
        echo '/goal/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id_game'  => 'required',
                'id_player'  => 'required',
                'goaltime'  => 'required',
            ]))
        {

            $model = new GoalModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'id_game' => $this->request->getPost('id_game'),
                'id_player' => $this->request->getPost('id_player'),
                'goaltime' => $this->request->getPost('goaltime'),
            ];

            $model->save($data);
            session()->setFlashdata('message', lang('Гол был успешно отредактирован!'));
            return redirect()->to('/game/view/'.$this->request->getPost('id_game'));
        }
        else
        {
            return redirect()->to('/goal/edit/'.$this->request->getPost('id'))->withInput();
        }
    }
    
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GoalModel();
        $data = $model->getGoal($id);
        $a = $data['id_game'];
        $model->delete($id);
        session()->setFlashdata('message', lang('Гол был успешно удален!'));
        return redirect()->to('/game/view/'.$a);
    }
}
