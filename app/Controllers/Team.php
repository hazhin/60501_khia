<?php namespace App\Controllers;

use App\Models\TeamModel;
use App\Models\PlayerModel;
use CodeIgniter\Controller;
use Aws\S3\S3Client;

class Team extends BaseController
{

    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TeamModel();
        $data ['team'] = $model->getTeam();
        echo view('team/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TeamModel();
        $data ['team'] = $model->getTeam($id);
        $model = new PlayerModel();
        $data ['player'] = $model->getPlayerByTeamId($id);
        echo view('team/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('team/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[255]',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',

            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new TeamModel();
            //подготовка данных для модели
            $data = [
                'name' => $this->request->getPost('name'),
            ];
            //если изображение было загружено и была получена ссылка на него то добавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_team'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Команда успешно добавлена!'));
            return redirect()->to('/team');
        }
        else
        {
            return redirect()->to('/team/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TeamModel();

        helper(['form']);
        $data ['team'] = $model->getTeam($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('team/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);
        echo '/team/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'name' => 'required|min_length[3]|max_length[255]',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',

            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new TeamModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),

            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_team'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Команда была успешно отредактирована!'));
            return redirect()->to('/team/view/'.$this->request->getPost('id'));
        }
        else
        {
            return redirect()->to('/team/edit/'.$this->request->getPost('id'))->withInput();
        }
    }
    
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TeamModel();
        $model->delete($id);
        session()->setFlashdata('message', lang('Команда была успешно удалена!'));
        return redirect()->to('/team');
    }
}
