<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<h2>Все игроки</h2>

<?php if (!empty($player) && is_array($player)) : ?>

    <?php foreach ($player as $item): ?>
        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                    <?php if (is_null($item['picture_url'])) : ?>
                        <?php if ($item['amplua'] == 'Защитник') : ?>
                           <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/1685/1685036.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php elseif ($item['amplua'] == 'Полузащитник') : ?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/166/166344.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php elseif ($item['amplua'] == 'Нападающий') : ?>
                        <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/1031/1031379.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php endif ?>
                    <?php else:?>
                        <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php endif ?>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($item['name']); ?></h5>
                        <p class="card-text"><?= esc($item['amplua']); ?></p>
                        <a href="<?= base_url()?>/player/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                    </div>
                </div>
            </div>
        </div>


    <?php endforeach; ?>

<?php else : ?>

    <p>Невозможно найти футболистов.</p>

<?php endif ?>
</div>
<?= $this->endSection() ?>
