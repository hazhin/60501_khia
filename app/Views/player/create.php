<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('player/store'); ?>
    <div class="form-group">
        <label for="name">Введите имя игрока</label>
        <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
               value="<?= old('name'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('name') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="id_team">Выберите команду</label>
        <select class="form-control" name="id_team">
            <?php
            foreach ($team as $row)
            {
               echo "<option value=".$row["id"].">".$row["name"]."</option>";
            }
            ?>
        </select> 
        <div class="invalid-feedback">
            <?= $validation->getError('id_team') ?>
        </div>
     </div>

    <div class="form-group">
        <label class="form-check-label">Амплуа</label>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="amplua" value="Защитник" <?= old('amplua')=='Защитник' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Защитник</small>
            </label>
        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="amplua" value="Полузащитник" <?= old('amplua')=='Полузащитник' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Полузащитник</small>
            </label>
        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="amplua" value="Нападающий" <?= old('amplua')=='Нападающий' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Нападающий</small>
            </label>
        </div>

        <div class="invalid-feedback" style="display: block">
            <?= $validation->getError('amplua') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="picture_url">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>

</div>
<?= $this->endSection() ?>
