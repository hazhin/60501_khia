<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($player)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                        <?php if (is_null($player['picture_url'])) : ?>
                            <?php if ($player['amplua'] == 'Защитник') : ?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/1685/1685036.svg" class="card-img" alt="<?= esc($player['name']); ?>">
                            <?php elseif ($player['amplua'] == 'Полузащитник') : ?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/166/166344.svg" class="card-img" alt="<?= esc($player['name']); ?>">
                            <?php elseif ($player['amplua'] == 'Нападающий') : ?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/1031/1031379.svg" class="card-img" alt="<?= esc($player['name']); ?>">
                            <?php endif ?>
                        <?php else:?>
                            <img height="150" src="<?= esc($player['picture_url']); ?>" class="card-img" alt="<?= esc($player['name']); ?>">
                        <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($player['name']); ?></h5>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Амплуа:</div>
                                <div class="text-muted"><?= esc($player['amplua']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Номер игрока:</div>
                                <div class="text-muted"><?= esc($player['id']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Команда:</div>
                                <?php
                                $db = \Config\Database::connect();
                                $sql = "SELECT * FROM team WHERE id = :id:";
                                $query = $db->query($sql, ['id' => $player["id_team"]]);
                                $row=$query->getRow();
                                $db->close();?>
                                <div class="text-muted"><?= esc($row->name); ?></div>
                            </div>
                            <p></p>
                            <div class="d-flex justify-content-between">
                            <a href="<?= base_url()?>/player/edit/<?= esc($player['id']); ?>" class="btn btn-primary">Редактировать</a>
                            <a href="<?= base_url()?>/player/delete/<?= esc($player['id']); ?>" class="btn btn-danger">Удалить</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php else : ?>
        <p>Футболист не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
