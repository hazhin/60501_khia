<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<?php if (!empty($player) && is_array($player)) : ?>
    <h2>Все игроки:</h2>
    <div class="d-flex justify-content-between mb-2">
    <?= $pager->links('group1','my_page') ?>
        <?= form_open('player/viewAllWithPlayers', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
        </form>
        <?= form_open('player/viewAllWithPlayers',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Введите игрока или команду" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>

    <table class="table table-striped text-center">
        <thead>
            <th scope="col" class="align-middle">ID игрока</th>
            <th scope="col" class="align-middle">Игрок</th>
            <th scope="col" class="align-middle"></th>
            <th scope="col" class="align-middle">Команда</th>
            <th scope="col" class="align-middle"></th>
            <th scope="col" class="align-middle">Амплуа</th>
            <th scope="col" class="align-middle">Управление</th>

        </thead>
        <tbody>
        <?php foreach ($player as $item): ?>
        <tr>
        <td><?= esc($item['p1']); ?></td>
        <td>
            <?php if (is_null($item['p2'])) : ?>
                <?php if ($item['p4'] == 'Защитник') : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/1685/1685036.svg" alt="<?= esc($item['p3']); ?>">
                <?php elseif ($item['p4'] == 'Полузащитник') : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/166/166344.svg" alt="<?= esc($item['p3']); ?>">
                <?php elseif ($item['p4'] == 'Нападающий') : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/1031/1031379.svg" alt="<?= esc($item['p3']); ?>">
                <?php endif ?>
            <?php else:?>
                <img height="50" src="<?= esc($item['p2']); ?>" alt="<?= esc($item['p3']); ?>">
            <?php endif ?>
        </td>
        <td><?= esc($item['p3']); ?></td>
        <td>
            <?php if (is_null($item['t2'])) : ?>
            <img height="50" width="50" src="https://seeklogo.com/images/P/premier-league-new-logo-D22A0CE87E-seeklogo.com.png" alt="<?= esc($item['t3']); ?>">
            <?php else:?>
            <img height="50" width="50" src="<?= esc($item['t2']); ?>" alt="<?= esc($item['t3']); ?>">
            <?php endif ?>  
        </td>
        <td><?= esc($item['t3']); ?></td>
        <td><?= esc($item['p4']); ?></td>
            <td>
                <a href="<?= base_url()?>/player/view/<?= esc($item['p1']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                <a href="<?= base_url()?>/player/edit/<?= esc($item['p1']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                <a href="<?= base_url()?>/player/delete/<?= esc($item['p1']); ?>" class="btn btn-danger btn-sm">Удалить</a>
            </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>

<?php else : ?>
    <div class="text-center">
    <p>Игроки не найдены </p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/player/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать игрока</a>
    </div>
<?php endif ?>
</div>
<?= $this->endSection() ?>
