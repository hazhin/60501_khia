<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<main class="text-center">
    <div class="jumbotron">
        <img class="mb-4" src="https://www.fifplay.com/img/public/premier-league-logo.png" alt="" width="315" height="133"><h1 class="display-4"></h1>
        <p class="lead">Этот сайт поможет вам следить за последними новостями в Английском чемпионате по футболу.</p>
        <?php if (! $ionAuth->loggedIn()): ?>
        <a class="btn btn-primary btn-lg" href="/auth/login" role="button">Войти</a>
        <?php endif ?>
    </div>
</main>
<?= $this->endSection() ?>
