<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('game/store'); ?>

    <div class="form-group">
        <label for="id_team1">Выберите команду хозяев</label>
        <select class="form-control" name="id_team1">
            <?php
            foreach ($team as $row)
            {
               echo "<option value=".$row["id"].">".$row["name"]."</option>";
            }
            ?>
        </select> 
        <div class="invalid-feedback">
            <?= $validation->getError('id_team1') ?>
        </div>
     </div>

    <div class="form-group">
        <label for="id_team2">Выберите команду гостей</label>
        <select class="form-control" name="id_team2">
            <?php
            foreach ($team as $row)
            {
               echo "<option value=".$row["id"].">".$row["name"]."</option>";
            }
            ?>
        </select> 
        <div class="invalid-feedback">
            <?= $validation->getError('id_team2') ?>
        </div>
     </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>

</div>
<?= $this->endSection() ?>
