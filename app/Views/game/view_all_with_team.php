<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<?php if (!empty($game) && is_array($game)) : ?>
    <h2>Все матчи:</h2>
    <div class="d-flex justify-content-between mb-2">
    <?= $pager->links('group2','my_page') ?>
        <?= form_open('game/viewAllWithTeam', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
        </form>
        <?= form_open('game/viewAllWithTeam',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Введите команду" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>

    <table class="table table-striped text-center">
        <thead>
            <th scope="col" class="align-middle">№ Матча</th>
            <th scope="col" class="align-middle">Хозяева</th>
            <th scope="col" class="align-middle"></th>
            <th scope="col" class="align-middle">Счет</th>
            <th scope="col" class="align-middle"></th>
            <th scope="col" class="align-middle">Гости</th>
            <th scope="col" class="align-middle">Управление</th>
        </thead>
        <tbody>
        
        <?php foreach ($game as $item): ?>
        <tr>
        <td><?= esc($item['q']); ?></td>
        <td><?= esc($item['r']); ?></td>
        <td>
            <?php if (is_null($item['y'])) : ?>
            <img height="50" src="https://seeklogo.com/images/P/premier-league-new-logo-D22A0CE87E-seeklogo.com.png" alt="<?= esc($item['r']); ?>">
            <?php else:?>
            <img height="50" src="<?= esc($item['y']); ?>" alt="<?= esc($item['r']); ?>">
            <?php endif ?>
        </td>
        <td><?= esc($item['w']); ?> : <?= esc($item['e']); ?></td>
        <td>
            <?php if (is_null($item['u'])) : ?>
            <img height="50" src="https://seeklogo.com/images/P/premier-league-new-logo-D22A0CE87E-seeklogo.com.png" alt="<?= esc($item['t']); ?>">
            <?php else:?>
            <img height="50" src="<?= esc($item['u']); ?>" alt="<?= esc($item['t']); ?>">
        <?php endif ?>
        </td>
        <td><?= esc($item['t']); ?></td>
            <td>
                <a href="<?= base_url()?>/game/view/<?= esc($item['q']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                <a href="<?= base_url()?>/game/edit/<?= esc($item['q']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                <a href="<?= base_url()?>/game/delete/<?= esc($item['q']); ?>" class="btn btn-danger btn-sm">Удалить</a>
            </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>

<?php else : ?>
    <div class="text-center">
    <p>Матчи не найдены </p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/game/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать матч</a>
    </div>
<?php endif ?>
</div>
<?= $this->endSection() ?>
