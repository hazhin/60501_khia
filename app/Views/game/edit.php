<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('game/update'); ?>
    <input type="hidden" name="id" value="<?= $game["id"] ?>">

    <div class="form-group">
        <label for="name">Изменить команду хозяев</label>
        <select class="form-control" name="id_team1">
            <?php
            $db = \Config\Database::connect();
            $sql = "select * from team where team.id!=:tid:";
            $query = $db->query($sql, ['tid' => $game["id_team1"]]);
            $team=$query->getResult('array');

            $sql = "select * from team where team.id=:tid:";
            $query = $db->query($sql, ['tid' => $game["id_team1"]]);
            $z=$query->getRow();
            $db->close();
            echo "<option value=".$z->id.">".$z->name."</option>";
            foreach ($team as $row)
            {
               echo "<option value=".$row["id"].">".$row["name"]."</option>";
            }
            ?>
        </select>         

        <div class="invalid-feedback">
            <?= $validation->getError('id_team1') ?>
        </div>

    </div>

    <div class="form-group">
        <label for="name">Изменить команду гостей</label>
        <select class="form-control" name="id_team2">
            <?php
            $db = \Config\Database::connect();
            $sql = "select * from team where team.id!=:tid:";
            $query = $db->query($sql, ['tid' => $game["id_team2"]]);
            $team=$query->getResult('array');

            $sql = "select * from team where team.id=:tid:";
            $query = $db->query($sql, ['tid' => $game["id_team2"]]);
            $z=$query->getRow();
            $db->close();
            echo "<option value=".$z->id.">".$z->name."</option>";
            foreach ($team as $row)
            {
               echo "<option value=".$row["id"].">".$row["name"]."</option>";
            }
            ?>
        </select>         

        <div class="invalid-feedback">
            <?= $validation->getError('id_team2') ?>
        </div>

    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
