<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<?php if (!empty($game) && is_array($game)) : ?>
<div class="row justify-content-center align-items-center ">
    <table class="table table-striped text-center">
        <thead>
            <th scope="col" width="20%">Хозяева</th>
            <th scope="col" width="20%"></th>
            <th scope="col" width="20%">Счет</th>
            <th scope="col" width="20%"></th>
            <th scope="col" width="20%">Гости</th>
        </thead>
        <tbody>
        <tr>

        <td class="align-middle" style="font-size: 25px;"><?= esc($game['r']); ?></td>
        <td>
            <?php if (is_null($game['y'])) : ?>
            <img height="120" src="https://seeklogo.com/images/P/premier-league-new-logo-D22A0CE87E-seeklogo.com.png" alt="<?= esc($game['r']); ?>">
            <?php else:?>
            <img height="120" src="<?= esc($game['y']); ?>" alt="<?= esc($game['r']); ?>">
            <?php endif ?>
        </td>
        <td class="align-middle" style="font-size: 55px;"><?= esc($game['w']); ?> : <?= esc($game['e']); ?></td>
        <td>
            <?php if (is_null($game['u'])) : ?>
            <img height="120" src="https://seeklogo.com/images/P/premier-league-new-logo-D22A0CE87E-seeklogo.com.png" alt="<?= esc($game['t']); ?>">
            <?php else:?>
            <img height="120" src="<?= esc($game['u']); ?>" alt="<?= esc($game['t']); ?>">
            <?php endif ?>
        </td>
        <td class="align-middle" style="font-size: 25px;"><?= esc($game['t']); ?></td>
        </tr>
        </tbody>
        </table>
</div>
        <?php if (!empty($goal) && is_array($goal)) : ?>
        <h4>Список игроков, забивших в этом матче: </h4>
        <br>
<div class="row justify-content-center align-items-center">        
        <table class="table table-striped text-center">
        <thead>
            <th scope="col" class="align-middle">Игрок</th>
            <th scope="col" class="align-middle"></th>
            <th scope="col" class="align-middle">Амплуа</th>
            <th scope="col" class="align-middle">Время гола</th>
            <th scope="col" class="align-middle">Управление</th>

        </thead>
        <tbody>
        <?php foreach ($goal as $item): ?>
        <tr>
        <td>
            <?php if (is_null($item['e'])) : ?>
                <?php if ($item['r'] == 'Защитник') : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/1685/1685036.svg" alt="<?= esc($item['w']); ?>">
                <?php elseif ($item['r'] == 'Полузащитник') : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/166/166344.svg" alt="<?= esc($item['w']); ?>">
                <?php elseif ($item['r'] == 'Нападающий') : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/1031/1031379.svg" alt="<?= esc($item['w']); ?>">
                <?php endif ?>
            <?php else:?>
                <img height="50" src="<?= esc($item['e']); ?>" alt="<?= esc($item['w']); ?>">
            <?php endif ?>
        </td>
        <td><?= esc($item['w']); ?></td>
        <td><?= esc($item['r']); ?></td>
        <td><?= esc($item['t']); ?> мин.</td>
            <td>
                <a href="<?= base_url()?>/goal/edit/<?= esc($item['y']); ?>" class="btn btn-primary btn-sm">Редактировать</a>
                <a href="<?= base_url()?>/goal/delete/<?= esc($item['y']); ?>" class="btn btn-danger btn-sm">Удалить</a>
            </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>
</div>
        <?php else : ?>
        <div class="text-center">
            <p>Голы не найдены </p>
        </div>
        <?php endif ?>
        <div class="text-center">
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/goal/create?gid=<?= esc($game['q']); ?>">Добавить гол</a>
        </div>
        <br>

<?php else : ?>
    <div class="text-center">
    <p>Матч не найден</p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/game/create">Создать матч</a>
    </div>
<?php endif ?>
</div>
<?= $this->endSection() ?>
