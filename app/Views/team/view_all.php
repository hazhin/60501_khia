<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<h2>Все команды</h2>

<?php if (!empty($team) && is_array($team)) : ?>

    <?php foreach ($team as $item): ?>

        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                        <?php if (is_null($item['picture_team'])) : ?>
                            <img height="150" width="150" src="https://seeklogo.com/images/P/premier-league-new-logo-D22A0CE87E-seeklogo.com.png" alt="<?= esc($item['name']); ?>">
                        <?php else:?>
                            <img height="150" width="150" src="<?= esc($item['picture_team']); ?>" alt="<?= esc($item['name']); ?>">
                        <?php endif ?>            
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($item['name']); ?></h5>
                        <a href="<?= base_url()?>/team/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach; ?>

<?php else : ?>

    <p>Невозможно найти команды.</p>

<?php endif ?>
</div>
<?= $this->endSection() ?>
