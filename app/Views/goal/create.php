<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <?php if(isset($_GET['gid'])):
            $gid=$_GET['gid'] ;
     endif; ?>
    <?php if(isset($_GET['gid'])):
    $db = \Config\Database::connect();
    $sql = "SELECT q,w,e from goal_create() WHERE e=:gid:";
    $query = $db->query($sql, ['gid' => $gid]);
    $z=$query->getRow();
    $goal=$query->getResult('array');
    $db->close();
    endif; ?>

    <?= form_open_multipart('goal/store'); ?>
    <input type="hidden" name="id_game" value="<? if(isset($z)): echo($z->e); endif;?>">

    <div class="form-group">
        <label for="id_player">Выберите игрока</label>
        <select class="form-control" name="id_player">
            <?php if(isset($_GET['gid'])):
            foreach ($goal as $row)
            {
               echo "<option value=".$row["w"].">".$row["q"]."</option>";
            }
            endif;?>
        </select> 
        <div class="invalid-feedback">
            <?= $validation->getError('id_player') ?>
        </div>
     </div>

    <div class="form-group">
        <label for="goaltime">Время, когда был забит гол (в минутах)</label>
        <input type="number" min="1" max="90" class="form-control <?= ($validation->hasError('goaltime')) ? 'is-invalid' : ''; ?>" name="goaltime">
        <div class="invalid-feedback">
            <?= $validation->getError('goaltime') ?>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>

</div>
<?= $this->endSection() ?>
