<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
<?php if (!empty($goal) && is_array($goal)) : ?>
    <?= form_open_multipart('goal/update'); ?>
    <input type="hidden" name="id" value="<?= $goal[1]["r"] ?>">
    <input type="hidden" name="id_game" value="<?= $goal[1]["e"] ?>">

    <div class="form-group">
        <label for="id_player">Изменить игрока, который забил гол</label>
        <select class="form-control" name="id_player">
    <?php if(isset($goal[1]["r"])):
    $db = \Config\Database::connect();
    $sql = "select player.id as c, concat(team.name, ' - ', player.name) as v, goal.goaltime as b from player, goal, team where goal.id_player=player.id and player.id_team=team.id and goal.id = :goid:";
    $query = $db->query($sql, ['goid' => $goal[1]["r"]]);
    $x=$query->getRow();
    $db->close();
    echo "<option value=".$x->c.">".$x->v."</option>";
    endif; ?>
   
            <?php foreach ($goal as $row)
            {
               echo "<option value=".$row["w"].">".$row["q"]."</option>";
            }
            endif;?>
        </select> 
        <div class="invalid-feedback">
            <?= $validation->getError('id_player') ?>
        </div>
     </div>

    <div class="form-group">
        <label for="goaltime">Изменить время, когда был забит гол</label>
        <input type="number" min="1" max="90" value="<?= esc($x->b); ?>" class="form-control <?= ($validation->hasError('goaltime')) ? 'is-invalid' : ''; ?>" name="goaltime">
        <div class="invalid-feedback">
            <?= $validation->getError('goaltime') ?>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
