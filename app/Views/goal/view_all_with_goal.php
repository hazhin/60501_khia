<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<?php if (!empty($goal) && is_array($goal)) : ?>
    <h2>Все голы:</h2>
    <div class="d-flex justify-content-between mb-2">
    <?= $pager->links('group3','my_page') ?>
        <?= form_open('goal/viewAllWithGoal', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
        </form>
        <?= form_open('goal/viewAllWithGoal',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Введите имя игрока" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>

    <table class="table table-striped">
        <thead>
            <th scope="col">№ Матча</th>
            <th scope="col">Игрок</th>
            <th scope="col"></th>
            <th scope="col">Амплуа</th>
            <th scope="col">Время гола</th>
            <th scope="col">Управление</th>

        </thead>
        <tbody>
        <?php foreach ($goal as $item): ?>
        <tr>
        <td><?= esc($item['q']); ?></td>
        <td>
            <?php if (is_null($item['e'])) : ?>
                <?php if ($item['r'] == 'Защитник') : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/1685/1685036.svg" alt="<?= esc($item['w']); ?>">
                <?php elseif ($item['r'] == 'Полузащитник') : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/166/166344.svg" alt="<?= esc($item['w']); ?>">
                <?php elseif ($item['r'] == 'Нападающий') : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/1031/1031379.svg" alt="<?= esc($item['w']); ?>">
                <?php endif ?>
            <?php else:?>
                <img height="50" src="<?= esc($item['e']); ?>" alt="<?= esc($item['w']); ?>">
            <?php endif ?>
        </td>
        <td><?= esc($item['w']); ?></td>
        <td><?= esc($item['r']); ?></td>
        <td><?= esc($item['t']); ?></td>
            <td>
                <a href="<?= base_url()?>/goal/edit/<?= esc($item['y']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                <a href="<?= base_url()?>/goal/delete/<?= esc($item['y']); ?>" class="btn btn-danger btn-sm">Удалить</a>
            </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>

<?php else : ?>
    <div class="text-center">
    <p>Голы не найдены </p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/goal/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать гол</a>
    </div>
<?php endif ?>
</div>
<?= $this->endSection() ?>
