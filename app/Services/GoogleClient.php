<?php namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('359974599033-1a1gri5le0lk625clei2je22cqr19t6c.apps.googleusercontent.com');
        $this->google_client->setClientSecret('l7lYtbH39PuEH5_s-mUPVVPO');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}
