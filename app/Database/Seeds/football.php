<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class football extends Seeder
{
	public function run()
	{
       
        $data = [
                
                'name'=> 'Арсенал',
        ];
        $this->db->table('team')->insert($data);

        $data = [
                
                'name'=> 'Челси',
        ];
        $this->db->table('team')->insert($data);

        $data = [
                
                'name'=> 'Эвертон',
        ];
        $this->db->table('team')->insert($data);

        $data = [
                
                'name'=> 'Лестер Сити',
        ];
        $this->db->table('team')->insert($data);

        $data = [
                
                'name'=> 'Ливерпуль',
        ];
        $this->db->table('team')->insert($data);

        $data = [
                
                'name'=> 'Манчестер Сити',
        ];
        $this->db->table('team')->insert($data);

        $data = [
                
                'name'=> 'Манчестер Юнайтед',
        ];
        $this->db->table('team')->insert($data);

        $data = [
                
                'name'=> 'Тоттенхэм Хотспур',
        ];
        $this->db->table('team')->insert($data);

        $data = [
                
                'id_team' => 1,
                'name' => 'Давид Луиз',
                'amplua'=>'Защитник',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p41270.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 1,
                'name' => 'Мартин Эдегор',
                'amplua'=>'Полузащитник',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p184029.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 1,
                'name' => 'Николя Пепе',
                'amplua'=>'Нападающий',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p195735.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 2,
                'name' => 'Бенджамин Чилвелл',
                'amplua'=>'Защитник',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p172850.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 2,
                'name' => 'Хаким Зиеш',
                'amplua'=>'Полузащитник',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p124183.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 2,
                'name' => 'Тимо Вернер',
                'amplua'=>'Нападающий',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p165153.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 3,
                'name' => 'Ерри Мина',
                'amplua'=>'Защитник',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p164511.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 3,
                'name' => 'Хамес Родригес',
                'amplua'=>'Полузащитник',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p60025.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 3,
                'name' => 'Ришарлисон Андраде',
                'amplua'=>'Нападающий',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p212319.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 4,
                'name' => 'Джейми Варди',
                'amplua'=>'Нападающий',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p101668.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 5,
                'name' => 'Садио Мане',
                'amplua'=>'Нападающий',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p110979.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 6,
                'name' => 'Серхио Агуэро',
                'amplua'=>'Нападающий',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p37572.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 7,
                'name' => 'Бруно Фернандеш',
                'amplua'=>'Полузащитник',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p141746.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
                'id_team' => 8,
                'name' => 'Харри Кейн',
                'amplua'=>'Нападающий',
                'picture_url'=>'https://resources.premierleague.com/premierleague/photos/players/250x250/p78830.png',
        ];
        $this->db->table('player')->insert($data);

        $data = [
            
            'id_team1'=> 1,
            'id_team2'=> 2,
        ];
        $this->db->table('game')->insert($data);
        
        $data = [
            
            'id_team1'=> 3,
            'id_team2'=> 4,
        ];
        $this->db->table('game')->insert($data);
        
                $data = [
            
            'id_team1'=> 5,
            'id_team2'=> 6,
        ];
        $this->db->table('game')->insert($data);

        $data = [
            
            'id_team1'=> 7,
            'id_team2'=> 8,
        ];
        $this->db->table('game')->insert($data);

        $data = [
            
            'id_game'=> 1,
            'id_player'=> 1,
            'goaltime'=> 33,
        ];
        $this->db->table('goal')->insert($data);

        $data = [
            
            'id_game'=> 2,
            'id_player'=> 10,
            'goaltime'=> 45,
        ];
        $this->db->table('goal')->insert($data);

        $data = [
            
            'id_game'=> 3,
            'id_player'=> 11,
            'goaltime'=> 12,
        ];
        $this->db->table('goal')->insert($data);

        $data = [
            
            'id_game'=> 4,
            'id_player'=> 12,
            'goaltime'=> 86,
        ];
        $this->db->table('goal')->insert($data);
         
	}
}

