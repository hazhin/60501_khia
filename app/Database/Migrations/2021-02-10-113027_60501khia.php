<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Football extends Migration
{
	public function up()
	{
		if (!$this->db->tableexists('team'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('team', TRUE);
        }

        // activity_type
        if (!$this->db->tableexists('player'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'id_team' => array('type' => 'INT', 'null' => FALSE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'amplua' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_team','team','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('player', TRUE);
        }

        // activity_type
        if (!$this->db->tableexists('game'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'id_team1' => array('type' => 'INT', 'null' => FALSE),
                'id_team2' => array('type' => 'INT', 'null' => FALSE),

            ));
            $this->forge->addForeignKey('id_team1','team','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('id_team2','team','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('game', TRUE);
        }
        
        // activity_type
        if (!$this->db->tableexists('goal'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'id_game' => array('type' => 'INT', 'null' => FALSE),
                'id_player' => array('type' => 'INT', 'null' => FALSE),
                'goaltime' => array('type' => 'INT', 'null' => FALSE)

            ));
            $this->forge->addForeignKey('id_game','game','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('id_player','player','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('goal', TRUE);
        }
	}

	public function down()
	{
		 $this->forge->droptable('goal');
         $this->forge->droptable('game');
         $this->forge->droptable('player');
         $this->forge->droptable('team');
	}
}
