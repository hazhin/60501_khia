<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpictureteam extends Migration
{
	public function up()
	{
        if ($this->db->tableexists('team'))
        {
            $this->forge->addColumn('team',array(
                'picture_team' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
	}

	public function down()
	{
		$this->forge->dropColumn('team', 'picture_team');
	}
}
